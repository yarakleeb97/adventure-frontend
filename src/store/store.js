import Vuex from "vuex";

const store = new Vuex.Store({
  state: {
    sidebarVisible: "",
    sidebarUnfoldable: false,
    activities_filters: [],
    age: null,
    region: null,
    interest: null,
    selectedInterest: [],
    selectedRegion: [],
    selectedOrganiser: [],
  },
  getters: {},
  mutations: {
    toggleSidebar(state) {
      state.sidebarVisible = !state.sidebarVisible;
    },
    toggleUnfoldable(state) {
      state.sidebarUnfoldable = !state.sidebarUnfoldable;
    },
    updateSidebarVisible(state, payload) {
      state.sidebarVisible = payload.value;
    },
    passFilterToActivity(state, payload) {
      state.activities_filters = payload.filters;
    },
    updateAgeFilter(state, payload) {
      state.age = payload.age;
    },
    updateRegionFilter(state, payload) {
      state.region = payload.region;
    },
    updateInterestFilter(state, payload) {
      state.interest = payload.interest;
    },
    updateselectedInterest(state, payload) {
      state.selectedInterest = payload.selectedInterest;
    },
    updateselectedRegion(state, payload) {
      state.selectedRegion = payload.selectedRegion;
    },
    updateselectedOrganiser(state, payload) {
      state.selectedRegion = payload.selectedOrganiser;
    },
  },
  actions: {},
});
export default store;
