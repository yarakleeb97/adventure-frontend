import { createApp, h } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import * as apolloProvider from "./apollo.provider";
import CoreuiVue from "@coreui/vue";
import CIcon from "@coreui/icons-vue";
import { iconsSet as icons } from "@/assets/icons";

import "@coreui/coreui/dist/css/coreui.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "bootstrap-icons/font/bootstrap-icons.css";
import "@/assets/css/style.css";
import VueGoodTablePlugin from "vue-good-table";
import "vue-good-table/dist/vue-good-table.css";
import CKEditor from "@ckeditor/ckeditor5-vue";
import VueDatePicker from "@vuepic/vue-datepicker";
import "@vuepic/vue-datepicker/dist/main.css";
const app = createApp({
  render: () => h(App),
});

app.use(router);
app.use(store);
app.use(CoreuiVue);
app.use(CKEditor);
app.component("VueDatePicker", VueDatePicker);
app.use(VueGoodTablePlugin);
app.provide("icons", icons);
app.component("CIcon", CIcon);
app.mount("#app");
app.use(apolloProvider.provider);
