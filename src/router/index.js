import { createRouter, createWebHistory } from "vue-router";
import AdminLayout from "@/layouts/AdminLayout";
import UserLayout from "@/layouts/UserLayout";
const routes = [
  {
    path: "/admin/login",
    name: "Login",
    component: () => import("@/views/admin/pages/Login.vue"),
  },
  {
    path: "/admin",
    name: "Admin",
    component: AdminLayout,
    redirect: "/admin/dashboard",
    children: [
      {
        path: "/admin/dashboard",
        name: "DashboardView",
        // meta: { requiresAuth: true },
        component: () => import("@/views/admin/pages/DashboardView.vue"),
      },
      {
        path: "/admin/interest",
        name: "Interest",
        component: () => import("@/views/admin/interest/Interests.vue"),
      },
      {
        path: "/admin/region",
        name: "Region",
        component: () => import("@/views/admin/region/Regions.vue"),
      },
      {
        path: "/admin/organiser",
        name: "Organiser",
        component: () => import("@/views/admin/organiser/Organisers.vue"),
      },
      {
        path: "/admin/organiser/create",
        name: "CreateOrganiser",
        component: () => import("@/views/admin/organiser/CreateOrganiser.vue"),
      },
      {
        path: "/admin/organiser/edit/:id",
        name: "EditOrganiser",
        component: () => import("@/views/admin/organiser/EditOrganiser.vue"),
      },
      {
        path: "/admin/user",
        name: "Users",
        component: () => import("@/views/admin/user/Users.vue"),
      },
      {
        path: "/admin/activity",
        name: "Activities",
        component: () => import("@/views/admin/activity/Activities.vue"),
      },
      {
        path: "/admin/activity/create",
        name: "CreateActivity",
        component: () => import("@/views/admin/activity/CreateActivity.vue"),
      },
      {
        path: "/admin/activity/edit/:id",
        name: "EditActivity",
        component: () => import("@/views/admin/activity/EditActivity.vue"),
      },
      {
        path: "/admin/setting",
        name: "Setting",
        component: () => import("@/views/admin/setting/Settings.vue"),
      },
      {
        path: "/admin/setting/edit/:id",
        name: "EditSetting",
        component: () => import("@/views/admin/setting/EditSetting.vue"),
      },
      {
        path: "/admin/support-message",
        name: "SupportMessage",
        component: () => import("@/views/admin/support/SupportMessages.vue"),
      },
      {
        path: "/admin/subscription",
        name: "Subscription",
        component: () => import("@/views/admin/subscription/Subscriptions.vue"),
      },
      {
        path: "/admin/slider",
        name: "Slider",
        component: () => import("@/views/admin/slider/Sliders.vue"),
      },
      {
        path: "/admin/slider/create",
        name: "CreateSlider",
        component: () => import("@/views/admin/slider/CreateSlider.vue"),
      },
      {
        path: "/admin/slider/edit/:id",
        name: "EditSlider",
        component: () => import("@/views/admin/slider/EditSlider.vue"),
      },
    ],
  },
  {
    path: "/",
    name: "User",
    redirect: "/",
    component: UserLayout,
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("@/views/user/HomeView.vue"),
      },
      {
        path: "/activities/type/:type?",
        name: "activities",
        component: () => import("@/views/user/ActivitiesView.vue"),
      },
      {
        path: "/activities/:id",
        name: "activity",
        component: () => import("@/views/user/ActivityView.vue"),
      },
      {
        path: "/activities/:id/reservation",
        name: "reservation",
        component: () => import("@/views/user/ReservationView.vue"),
      },
      {
        path: "/organisers",
        name: "organisers",
        component: () => import("@/views/user/OrganisersView.vue"),
      },
      {
        path: "/organisers/:id",
        name: "organiser",
        component: () => import("../views/user/OrganiserView.vue"),
      },
      {
        path: "/terms-condtions",
        name: "terms",
        component: () => import("@/views/user/TermsView.vue"),
      },
      {
        path: "/privacy-policy",
        name: "privacy",
        component: () => import("@/views/user/PrivacyView.vue"),
      },
    ],
  },
  {
    path: "/",
    name: "Auth",
    redirect: "/",
    children: [
      {
        path: "/login",
        name: "login",
        component: () => import("@/views/user/LoginView.vue"),
      },
      {
        path: "/register",
        name: "register",
        component: () => import("@/views/user/RegisterView.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
