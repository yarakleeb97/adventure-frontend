export default [
  {
    component: "CNavItem",
    name: "Dashboard",
    to: "/admin/dashboard",
    icon: "cil-speedometer",
  },
  {
    component: "CNavItem",
    name: "Activity",
    to: "/admin/activity",
    icon: "cil-Diamond",
    items: [
      {
        component: "CNavItem",
        name: "Activity",
        to: "/admin/activity",
        icon: "cil-Golf",
      },
      {
        component: "CNavItem",
        name: "Interest",
        to: "/admin/interest",
        icon: "cil-AmericanFootball",
      },
      {
        component: "CNavItem",
        name: "Region",
        to: "/admin/region",
        icon: "cil-GlobeAlt",
      },
      {
        component: "CNavItem",
        name: "Organiser",
        to: "/admin/organiser",
        icon: "cil-Group",
      },
    ],
  },
  {
    component: "CNavItem",
    name: "User Managment",
    to: "/admin/user",
    icon: "cil-Group",
    items: [
      {
        component: "CNavItem",
        name: "User",
        to: "/admin/user",
        icon: "cil-user",
      },
      {
        component: "CNavItem",
        name: "subscribers",
        to: "/admin/subscription",
        icon: "cil-Gem",
      },
      {
        component: "CNavItem",
        name: "SupportMessage",
        to: "/admin/support-message",
        icon: "cil-Envelope-Closed",
      },
    ],
  },
  {
    component: "CNavItem",
    name: "Setting Managment",
    to: "/admin/setting",
    icon: "cil-Apps-Settings",
    items: [
      {
        component: "CNavItem",
        name: "Setting",
        to: "/admin/setting",
        icon: "cil-Settings",
      },
      {
        component: "CNavItem",
        name: "Slider",
        to: "/admin/slider",
        icon: "cil-Image",
      },
    ],
  },
];
