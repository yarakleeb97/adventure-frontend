import { createToast } from "mosha-vue-toastify";
import "mosha-vue-toastify/dist/style.css";

export function successToast(title) {
  createToast(
    {
      title: title,
    },
    { type: "success", showIcon: true }
  );
}
export function failedToast(title) {
  createToast(
    {
      title: title,
    },
    { type: "danger", showIcon: true }
  );
}
export function isEmpty(str) {
  return str == null || str === "";
}
export function changeFormatDate(str) {
  var date = new Date(str),
    year = date.getFullYear(),
    month = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2),
    hours = ("0" + date.getHours()).slice(-2),
    minutes = ("0" + date.getMinutes()).slice(-2),
    seconds = ("0" + date.getSeconds()).slice(-2);

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

export function getRemoteData(
  multiple = false,
  data,
  processResults,
  placeholder = "Choose values"
) {
  return {
    allowClear: true,
    placeholder: placeholder,
    ajax: {
      method: "POST",
      url: "http://127.0.0.1:8000/graphql",
      contentType: "application/json",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("USER_TOKEN"),
      },
      data: data,
      processResults: processResults,
      initSelection: function (element, callback) {
        // Set the selected value here
        var selectedValue = { id: 1, text: "Selected Value" };
        callback(selectedValue);
      },
    },
    multiple: multiple,
    defaults: function () {
      return [
        {
          id: 1,
          text: "test",
          selected: true,
        },
      ];
    },
  };
}
