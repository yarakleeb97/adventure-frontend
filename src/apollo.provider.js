// import { ApolloClient, InMemoryCache, split } from "@apollo/client/core";
import { ApolloClient, InMemoryCache } from "@apollo/client/core";
import { createApolloProvider } from "@vue/apollo-option";
// import { setContext } from "apollo-link-context";
import { createUploadLink } from "apollo-upload-client";

const cache = new InMemoryCache();

const httpLink = new createUploadLink({
  uri: "http://127.0.0.1:8000/graphql",
});
// const httpLinkAuth = setContext((_, { headers }) => {
//   // get the authentication token from localstorage if it exists
//   const token = localStorage.getItem("USER_TOKEN");

//   // return the headers to the context so httpLink can read them
//   return {
//     headers: {
//       ...headers,
//       Authorization: `Bearer ${token}`,
//     },
//   };
// });

const apolloClient = new ApolloClient({
  cache: cache,
  // link: httpLinkAuth.concat(split(httpLink)),
  link: httpLink,
});

export const provider = createApolloProvider({
  defaultClient: apolloClient,
});
