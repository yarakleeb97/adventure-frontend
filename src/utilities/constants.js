const TYPE = ["CAMP", "EVENT"];
const FILTERS = ["age", "interest", "region", "date", "month"];
const MONTHS = {
  1: "Jan",
  2: "Feb",
  3: "Mar",
  4: "Apr",
  5: "May",
  6: "Jun",
  7: "Jul",
  8: "Aug",
  9: "Sep",
  10: "Oct",
  11: "Nov",
  12: "Dec",
};
const BaseUrl = "http://127.0.0.1:8000/";
const AGE_LEVELS = ["Childs", "Teenagers", "Adults"];
export { TYPE, MONTHS, AGE_LEVELS, FILTERS, BaseUrl };
